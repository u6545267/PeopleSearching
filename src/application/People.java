package application;

public class People implements Comparable<People> {
    String name;
    String age;
    String sex;

    People() {}

    People(
            String name,
            String age,
            String sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    String getName() {
        return name;
    }

    String getAge() {
        return age;
    }

    String getSex() {
        return sex;
    }

    @Override
    public String toString() {
        return name + " " + age + " " + sex;
    }

    @Override
    public boolean equals(Object o) {

        if (o == null) return false;
        if (!(o instanceof People)) return false;

        People other = (People) o;
        String lname = this.name.toLowerCase();
        String olname = other.name.toLowerCase();

        /*DEBUG INFO*/
        // '==' compares context as well as reference
        // while 'equals()' just compares the context
        // we should use 'equals()'
        /*System.out.println(
                (olname+" "+other.age+" "+other.sex)+
                " ("+(lname.equals(olname))+" && "+(this.age.equals(other.age))+" && "+
                (this.sex.equals(other.sex))+") -> "+
                (lname.equals(olname)
                        && this.age.equals(other.age)
                        && this.sex.equals(other.sex)));*/
        /*DEBUG INFO*/

        return
                lname.equals(olname) &&
                this.age.equals(other.age) &&
                this.sex.equals(other.sex);
    }

    @Override
    public int hashCode() {
        return name.hashCode()+
                age.hashCode()+
                sex.hashCode();
    }

    @Override
    public int compareTo(People o) {
        return name.compareToIgnoreCase(o.name);
    }
}
