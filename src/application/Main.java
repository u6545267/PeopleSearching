package application;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class Main extends Application {
    String name;
    String age;
    String sex;
    @Override
    public void start(Stage stage) {
        try {
            stage.setTitle("ANUC1110 Lab7");
            stage.setResizable(false);

            VBox root = new VBox();
            root.setSpacing(10);
            root.setAlignment(Pos.CENTER);
            root.setPadding(new Insets(15, 15, 15, 15));

            HBox hbTitle = new HBox();
            HBox hbName = new HBox(5);
            HBox hbAge = new HBox(5);
            HBox hbGender = new HBox(5);

            HBox hbFields = new HBox();
            hbFields.getChildren().addAll(hbName, hbAge, hbGender);

            Text textTitle = new Text("People Class Demonstration");
            textTitle.setId("title");
            hbTitle.getChildren().add(textTitle);
            hbTitle.setAlignment(Pos.CENTER);

            Text textName = new Text("Name:");
            TextField fieldName = new TextField();
            fieldName.setPrefWidth(200);
            hbName.getChildren().addAll(textName, fieldName);
            hbName.setAlignment(Pos.CENTER);
            hbName.setPadding(new Insets(0,5,0,0));

            Text textAge = new Text("Age:");
            TextField fieldAge = new TextField(null);
            fieldAge.setPrefWidth(120);
            hbAge.getChildren().addAll(textAge, fieldAge);
            hbAge.setAlignment(Pos.CENTER);
            hbAge.setPadding(new Insets(0,5,0,5));

            Text textGender = new Text("Gender:");
            ChoiceBox<String> choiceSex = new ChoiceBox<>();
            choiceSex.getItems().add("F");
            choiceSex.getItems().add("M");
            choiceSex.setPrefWidth(120);
            hbGender.getChildren().addAll(textGender, choiceSex);
            hbGender.setAlignment(Pos.CENTER);
            hbGender.setPadding(new Insets(0,5,0,5));

            Button btnAdd = new Button("Add");
            Button btnSort = new Button("Sort");
            Button btnPrint = new Button("Print");
            Button btnSearch = new Button("Search");
            Button btnDelete = new Button("Delete");
            HBox hbBtns = new HBox(10);
            hbBtns.setAlignment(Pos.CENTER);
            hbBtns.getChildren().addAll(btnAdd, btnSort, btnPrint, btnSearch, btnDelete);

            Text textDetails = new Text("Details:");
            HBox hbDetails = new HBox();
            hbDetails.setAlignment(Pos.BASELINE_LEFT);
            hbDetails.getChildren().add(textDetails);

            Label label = new Label();
            label.setPadding(new Insets(10,10,10,10));
            label.setAlignment(Pos.TOP_LEFT);
            label.setId("label");

            root.getChildren().addAll(hbTitle, hbFields, hbBtns, hbDetails, label);

            //      Set<People> list = new HashSet<>();
            List<People> list = new ArrayList<>();
            list.add(new People("Jackson","21","M"));
            list.add(new People("Mary","17","F"));
            list.add(new People("Glen","24","F"));
            list.add(new People("Hannah","23","F"));
            list.add(new People("Max","10","M"));
            list.add(new People("Andy","18","M"));

            // TODO implement text field restrictions
            btnAdd.setOnAction(e -> {
                Object a = fieldName.getText();
                Object b = fieldAge.getText();
                Object c = choiceSex.getValue();
                // user must complete all the fields before adding
                if (a != null &&
                        b != null &&
                        c != null) {
                    name = fieldName.getText();
                    age = fieldAge.getText();
                    sex = (String) choiceSex.getValue();
                    People person = new People(name, age, sex);
                    // check if the record already exists
                    if (!list.contains(person)) {
                        list.add(person);
                        label.setText("Record (" + person.toString()
                        + ") has been added sucessfully!");
                        label.setTextFill(Color.GREEN);
                    } else {
                        label.setText("The same record already exists!");
                        label.setTextFill(Color.FIREBRICK);
                    }
                } else {
                    label.setText("Please complete all fields before adding!");
                    label.setTextFill(Color.FIREBRICK);
                }
            });

            btnSort.setOnAction(e -> {
                Collections.sort(list);
                print(list, label);
            });

            btnPrint.setOnAction(e -> {
                print(list, label);
            });

            btnSearch.setOnAction(e -> {
                name = fieldName.getText().toLowerCase();
                String output = "";
                int indeix = 1;
                for (People p : list) {
                    if (p.getName().toLowerCase().contains(name)) {
                        output += indeix + ". " + p.toString()+"\n";
                        indeix++;
                    }
                }
                String cap;
                if (indeix == 1) cap = "No record found.\n";
                else cap = indeix-1 + " record(s) found.\n";
                label.setText(cap+output);
                label.setTextFill(Color.BLACK);
            });

            btnDelete.setOnAction(e -> {
                List<People> listToBeRemoved = new ArrayList<>();
                name = fieldName.getText().toLowerCase();
                int index = 0;
                for (People p : list) {
                    if (p.getName().toLowerCase().equals(name)) {
                        listToBeRemoved.add(p);
                        index++;
                    }
                }
                list.removeAll(listToBeRemoved);
                String cap;
                if (index == 0) cap = "No record found.\n";
                else cap = index + " record(s) deleted.\n";
                label.setText(cap);
                label.setTextFill(Color.BLACK);
            });

            Scene scene = new Scene(root,450,500);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            stage.setScene(scene);
            stage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void print(List<People> list, Label label) {
        int size = list.size();
        int index = 1;
        String output = "There are " + size + " people in the list:\n";
        for (People p : list) {
            output += index + ". " + p.toString()+"\n";
            index++;
        }
        label.setText(output);
        label.setTextFill(Color.BLACK);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
